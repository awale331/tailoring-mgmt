@extends('layouts.layout')
@section('content')
        <div class="content">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">
                                    @include('inc.message')
                                    <h4 class="title">ADD WORKER</h4>
                                </div>
                                <div class="content">
                                    <form method="post" action="{{action('WorkerController@store')}}">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Worker's Full Name</label>
                                                    <input type="text" class="form-control" placeholder="Worker's Name" name="name" value="{{old('name')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input type="text" class="form-control" placeholder="Worker's Address" name="address" value="{{old('address')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Contact Number</label>
                                                    <input type="number" min="0"  class="form-control" placeholder="Worker's Phone Number" name="phone_number" value="{{old('phone_number')}}">
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-info btn-fill pull-right">Add Worker</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#worker-create').addClass('active');
        });
    </script>
@endsection


