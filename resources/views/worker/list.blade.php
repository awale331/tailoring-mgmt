@extends('layouts.layout')
@section('content')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                @include('inc.message')
                                <h4 class="title">Moods Tailoring</h4>
                                <p class="category">List of worker's in MOODS TAILORING</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover" id="workerTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone No.</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($lists as $list)
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->address}}</td>
                                                <td>{{$list->phone_number}}</td>
                                                <td><a href="{{ action('WorkerController@edit',$list->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <form onsubmit="return confirm('Are you sure to delete?')" action="{{ action('WorkerController@destroy', $list->id)}}" method="post" style="display: inline">
                                                        @csrf
                                                        <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

@endsection
@section('script')
    <script>
        $(function() {
            $('#worker-list').addClass('active');
        });

        $(document).ready( function () {
            $('#workerTable').DataTable();
        } );
    </script>
@endsection