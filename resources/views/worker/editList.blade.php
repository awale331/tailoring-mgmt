@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        @include('inc.message')
                        <div class="card">
                            <div class="header">
                                <h4 class="title">EDIT WORKER</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="{{action('WorkerController@update',$old->id)}}">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Company (disabled)</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Moods Complete Tailoring House">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Worker's Full Name</label>
                                                <input type="text" class="form-control" placeholder="Worker's Name" value="{{$old->name}}" name="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" class="form-control" placeholder="Worker's Address" value="{{$old->address}}" name="address">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <input type="number" min="0" class="form-control" placeholder="Worker's Phone Number" value="{{$old->phone_number}}" name="phone_number">
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-dark btn-fill ">Update</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#worker-create').addClass('active');
        });
    </script>
@endsection


