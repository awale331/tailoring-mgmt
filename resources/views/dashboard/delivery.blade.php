{{--@dd($todaysDelivery)--}}
<div class="content table-responsive table-full-width">
    <table class="table table-hover" id="deliveryTable">
        <thead>
        <tr>
            <th>Bill number</th>
            <th>Customer name</th>
            <th>Customer phone number</th>
            <th>Items</th>
        </tr>
        </thead>
        @foreach($todayDeliveries as $Delivery)
        <tbody>
            <tr>
                <td>{{$Delivery->bill_number}}</td>
                <td>{{$Delivery->customer_name}}</td>
                <td>{{$Delivery->customer_phone_number}}</td>
                <td>{{$Delivery->items}}</td>
            </tr>
        </tbody>
            @endforeach
    </table>
</div>
