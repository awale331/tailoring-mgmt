<div class="content table-responsive table-full-width">
    <table class="table table-hover" id="collectionTable">
        <thead>
        <tr>
            <th>Bill number</th>
            <th>Customer name</th>
            <th>Worker's name</th>
            <th>Worker's phone number</th>
            <th>Item</th>
        </tr>
        </thead>
        <tbody>
        @foreach($todayCollections as $collection)
            <tr>
                <td>{{$collection->bill_number}}</td>
                <td>{{$collection->customer_name}}</td>
                <td>{{$collection->worker}}</td>
                <td>{{$collection->phone_number}}</td>
                <td>{{$collection->item}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
