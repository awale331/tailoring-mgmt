<div class="content table-responsive table-full-width">
    <table class="table table-hover" id="trialTable">
        <thead>
        <tr>
            <th>Bill number</th>
            <th>Customer name</th>
            <th>Customer phone number</th>
            <th>Items</th>
        </tr>
        </thead>
        <tbody>
        @foreach($todayTrials as $trial)
            <tr>
                <td>{{$trial->bill_number}}</td>
                <td>{{$trial->customer_name}}</td>
                <td>{{$trial->customer_phone_number}}</td>
                <td>{{$trial->items}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
