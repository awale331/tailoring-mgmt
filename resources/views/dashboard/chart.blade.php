@if(count($salesAndRevenue)>0)
    <div class="col-md-12">
        <div class="card ">
            <div class="header">
                <h4 class="title">Sales</h4>
                <p class="category">Last 7 Days.</p>
            </div>
            <div class="content table-responsive table-full-width">
                <div id="column-chart" style="height:300px;"></div>

                <div class="footer">
                    <div class="legend">
                        <i class="fa fa-circle" style="color: #4283f4;"></i> Sales
                        <i class="fa fa-circle" style="color:#f44242;"></i> Revenue
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="col-md-12">
        <div class="text-center bg-primary"> No sales data to display</div>
    </div>
@endif