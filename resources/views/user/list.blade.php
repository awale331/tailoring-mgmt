@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-plain">
                        <div class="header">
                            @include('inc.message')
                            <h4 class="title">Moods User's</h4>
                            <p class="category">List of user's</p>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover" id="workerTable">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone No.</th>
                                    <th></th>
                                </tr>
                                </thead>
                                @foreach($lists as $list)
                                    <tbody>
                                    <tr>
                                        <td>{{$list->name}}</td>
                                        <td>{{$list->phone_number}}</td>
                                        <td><a href="{{ route('user_edit' ,$list->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                            <form onsubmit="return confirm('Are you sure to delete?')" action="{{ route('user_destroy' ,$list->id) }}" method="post" style="display: inline">
                                                @csrf
                                                <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(function() {
            $('#user-list').addClass('active');
        });

        $(document).ready( function () {
            $('#userTable').DataTable();
        } );
    </script>
@endsection