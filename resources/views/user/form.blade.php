@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                @include('inc.message')
                                <h4 class="title">ADD USER</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="{{ route('user_store') }}">
                                    @csrf
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label>Company (disabled)</label>--}}
                                                {{--<input type="text" class="form-control" disabled placeholder="Company" value="Moods Complete Tailoring House">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>User's Full Name</label>
                                                <input type="text" class="form-control" placeholder="User's Name" name="name" value="" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" placeholder="Password" name="password" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Contact Number</label>
                                                <input type="number" class="form-control" placeholder="User's Phone Number" name="phone_number" required>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info btn-fill pull-right">Add User</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#user-create').addClass('active');
        });
    </script>
@endsection


