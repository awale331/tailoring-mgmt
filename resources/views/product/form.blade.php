@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                @include('inc.message')
                                <h4 class="title">ADD PRODUCT</h4>
                            </div>
                            <div class="content">
                                <form action="{{ route('product_store') }}" method="post" >
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Product's Name</label>
                                                <input type="text" name="name" id="productName" class="form-control" placeholder="Product's Name" value="{{old('name')}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Wage Rate</label>
                                                <input type="number" name="wage_rate" id="wage_rate" class="form-control" placeholder="Wage Rate" value="{{old('wage_rate')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Product's Description</label>
                                                <textarea class ="form-control" name="description" id="description">{{old('description')}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info btn-fill pull-right">Add Product</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function() {
            $('#product-create').addClass('active');
        });
    </script>
@endsection
