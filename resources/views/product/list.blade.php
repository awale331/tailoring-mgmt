@extends('layouts.layout')
@section('content')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                @include('inc.message')
                                <h4 class="title">Product Descriptions</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="productTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Wage Rate</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($lists as $list)
                                            <tr>
                                                <td>{{$list->name}}</td>
                                                <td>{{$list->wage_rate}}</td>
                                                <td>{{$list->description}}</td>
                                                <td>
                                                    <a href="{{ route('product_update' ,$list->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <form onsubmit="return confirm('Are you sure you want to delete?')" action="{{ route('product_destroy' ,$list->id) }}" method="post" style="display:inline">
                                                    @csrf
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

@endsection
@section('script')
    <script>
        $(function() {
        $('#product-list').addClass('active');
        });

        $(document).ready( function () {
            $('#productTable').DataTable();
        } );
    </script>
@endsection
