<footer class="footer">
    <div class="container-fluid">
        {{--<nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        Company
                    </a>
                </li>
                <li>
                    <a href="#">
                        Portfolio
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
            </ul>
        </nav>--}}
        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> Developed with <i class="fa fa-heart" style="color: red;"> </i> by <a href="https://www.facebook.com/sarilotechnology/" target="blank">Sarilo Technology</a>
        </p>
    </div>
</footer>
