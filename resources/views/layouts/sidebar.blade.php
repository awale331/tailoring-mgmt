<div class="sidebar" data-color="purple" data-image="{{asset('img/sidebar-5.jpg')}}">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ route('dashboard') }}" class="simple-text">
                Moods
            </a>
        </div>

        <ul class="nav">
            <li id="dashboard" class="">
                <a href="{{ route('dashboard') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li id="bill-create">
                <a href="{{ route('bill_create') }}">
                    <i class="pe-7s-plus"></i>
                    <p>Create Bill</p>
                </a>
            </li>

            <li id="bill-list">
                <a href="{{ route('bill_list') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Bill List</p>
                </a>
            </li>

            <li id="order-list">
                <a href="{{ route('order_list') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Work Assignment</p>
                </a>
            </li>

            <li id="product-create">
                <a href="{{ route('product_create') }}">
                    <i class="pe-7s-plus"></i>
                    <p>Create Product</p>
                </a>
            </li>

            <li id="product-list">
                <a href="{{ route('product_list') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Product List</p>
                </a>
            </li>

            <li id="worker-create">
                <a href="{{ route('worker_create') }}">
                    <i class="pe-7s-add-user"></i>
                    <p>Create Worker</p>
                </a>
            </li>

            <li id="worker-list">
                <a href="{{ route('worker_list') }}">
                    <i class="pe-7s-note2"></i>
                    <p>Worker List</p>
                </a>
            </li>

            @if(Session::get('userInstance.role')=='MANAGER' || Session::get('userInstance.role')=='ADMIN')
            <li id="user-create">
                <a href="{{ route('user_create') }}">
                    <i class="pe-7s-add-user"></i>
                    <p>Create User</p>
                </a>
            </li>
            @endif

            <li id="user-list">
                <a href="{{ route('user_list') }}">
                    <i class="pe-7s-note2"></i>
                    <p>User List</p>
                </a>
            </li>

        </ul>
    </div>
</div>
