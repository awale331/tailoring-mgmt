<!--//for Message-->
@if(count($errors)>0)
    @foreach($errors->all() as $error)
        <div class='alert alert-danger'>
            {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
@endif
<!--for session success message-->
@if(session('status'))
    <div class='alert alert-success' style="text-align:left;" role="alert">
        {{session('status')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr>
@elseif(session('success'))
    <div class='alert alert-success' style="text-align:left;" role="alert">
        {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr>
@elseif(session('delete'))
    <div class='alert alert-danger' style="text-align:left;" role="alert">
        {{session('delete')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr>
@endif
<!--for session failed message-->
@if(session('error'))
    <div class='alert  alert-danger'>
        {{session('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif