@extends('layouts.layout')

@section("stylesheet")
    <script>
        $(document).ready(function(){
            $(function() {
                $('#bill-create').addClass('active');
            });
        });
    </script>
@endsection

@section('content')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                @include("inc.message")

                                @if($bill->total_amount-$payment ==  0)
                                <div class='alert alert-success' style="text-align:left;" role="alert">
                                    {{$bill->customer_name}} has already made full payment
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                <h4 class="title">Update Bill</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="{{ route('bill_update' ,$bill->id) }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Bill Number</label>
                                                <input type="number" name="bill_number" class="form-control" placeholder="Enter bill number" value="{{$bill->bill_number}}" required readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Customer's Name</label>
                                                <input type="text" name="customer_name" class="form-control" placeholder="Enter name" value="{{$bill->customer_name}}" required readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Customer's Contact Number</label>
                                                <input type="number" name="customer_number" class="form-control" placeholder="Enter phone number" value="{{$bill->customer_phone_number}}" required readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Particulars</label>
                                                <div class = "col-md-12">
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th>Product name</th>
                                                            <th>Quantity</th>
                                                            <th>Wage rate per unit</th>
                                                        </tr>
                                                        @foreach($orders as $order)
                                                            @if($order->bill_id == $bill->id)
                                                            <tr>
                                                                <td>
                                                                        @foreach($products as $product)
                                                                            @if($product->id == $order->product_id)
                                                                               {{$product->name}}
                                                                            @endif
                                                                        @endforeach
                                                                </td>
                                                                <td>{{$order->quantity}}</td>
                                                                <td>{{$order->wage_rate}}</td>
                                                            @endif
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Total Amount</label>
                                                <input type="number" name="total_amount" class="form-control" id = "totalAmount" placeholder="Enter total amount" value="{{$bill->total_amount}}" min=0 value="0" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Paid Amount</label>
                                                <input type="number" name="paid_amount" class="form-control" id = "paidAmount" placeholder="Enter total amount" value="{{$payment}}" min=0 value="0" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    @if($bill->total_amount-$payment > 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>New Payment</label>
                                                <input type="number" name="new_amount" class="form-control"  value="{{$bill->total_amount-$payment}}" min=0  max="{{$bill->total_amount-$payment}}" placeholder="Enter paid amount">
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Trial Date</label>
                                                <input type="date" name="trial_date" class="form-control" value="{{$od->trial_date}}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Delivery Date</label>
                                                <input type="date" name="delivery_date" class="form-control" value="{{$od->delivery_date}}" readonly>
                                            </div>
                                        </div>
                                    </div>



                                    <button type="submit" class="btn btn-info btn-fill pull-right">Update Bill</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

@endsection


