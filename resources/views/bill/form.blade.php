@extends('layouts.layout')

@section("stylesheet")
    <script>

        var wagerates = {@foreach($products as  $product){{$product->id.":".$product->wage_rate.","}}@endforeach};

        function updateAmounts(){
            updateWageAmount();
            updateSurplusAmount();
            updateAmountDue();
        }

        function updateWageAmount(){
            var i =1;
            var total = 0;
            for(i=1; i<count; i++){
                var quantitySelector = "quantity"+i;
                var wageSelector = "wage"+i;
                total = total + (document.getElementById(quantitySelector).value * (Number(document.getElementById(wageSelector).value)));
            }
            $("#totalWageAmount").val(total);
        }

        function updateSurplusAmount(){
            var totalWageAmount = document.getElementById("totalWageAmount").value;
            var totalAmount = document.getElementById("totalAmount").value;
            var surplusAmount = totalAmount - totalWageAmount;
            $("#SurplusAmount").val(surplusAmount);
        }

        function updateAmountDue(){
            var totalAmount = document.getElementById("totalAmount").value;
            var paidAmount = document.getElementById("paidAmount").value;
            var amountDue = totalAmount - paidAmount;
            $("#AmountDue").val(amountDue);
        }


        function changeWageRate(event) {
            var row = event.id.charAt(4)
            var selector = "#wage"+row;
            $(selector).val(wagerates[event.value]);
            updateAmounts();
        }

        $(document).ready(function(){
            $(function() {
                $('#bill-create').addClass('active');
            });

            count = 1;

            $(document).on('click', '.add', function(){
                var html = '';
                html += '<tr>';
                html += "<td><select class='form-control' name='products[]' id ='name"+count+"' onchange=\"changeWageRate\(this\)\">";
                html += "<option disabled>Product name</option>";
                @foreach($products as $product)
                    html += "<option value={{$product->id}}>{{$product->name}}</option>";
                @endforeach
                    html += "</select></td>";
                html += "<td><input type='number' id ='quantity"+count+"' class='form-control' name = 'quantity[]' onblur='updateAmounts\(\)' placeholder='Qty' min=1 required value=1 /></td>";
                html += "<td><input type='number' id ='wage"+count+"' class='form-control' name = 'wage_rate[]' placeholder='Wage Rate'  value ="+wagerates[1]+" readonly min=0></td>";
                html += "<td><button type='button' name='remove' class='btn btn-danger btn-sm remove' min=0 ><span class='glyphicon glyphicon-minus'></span></button></td></tr>";
                $('#item_table').append(html);
                count++;
                updateAmounts();
            });

            $(document).on('click', '.remove', function(){
                count = count -1;
                $(this).closest('tr').remove();
                updateAmounts();
            });
        });
    </script>
@endsection

@section('content')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                @include("inc.message")
                                <h4 class="title">Register Bill</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="{{ route('bill_store') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Bill Number</label>
                                                <input type="number" name="bill_number" class="form-control" placeholder="Enter bill number" value="{{(old('bill_number'))?old('bill_number'):$billNumber}}" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Customer's Name</label>
                                                <input type="text" name="customer_name" value="{{old('customer_name')}}" class="form-control" placeholder="Enter name" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Customer's Contact Number</label>
                                                <input type="number" name="customer_number" value = "{{old('customer_number')}}" class="form-control" placeholder="Enter phone number" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Trial Date</label>
                                                <input type="date" name="trial_date" value = "{{old('trial_date')}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Delivery Date</label>
                                                <input type="date" name="delivery_date" value = "{{old('delivery_date')}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">                       
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Particulars</label>
                                                <div class = "col-md-12">
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th>Product name</th>
                                                            <th>Quantity</th>
                                                            <th>Wage rate per unit</th>
                                                            <th><button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Total Wage Amount</td>
                                                    <td><input type="number" id = "totalWageAmount" class="form-control"  value="0" disabled></td>
                                                </tr>
                                                <tr>
                                                    <td>Surplus Amount</td>
                                                    <td><input type="number" id = "SurplusAmount"  class="form-control" value = "0" disabled></td>
                                                </tr>
                                                <tr>
                                                    <th>Total Amount</th>
                                                    <td><input type="number" id = "totalAmount" name="total_amount" class="form-control" onkeyup="updateAmounts()" placeholder="Enter total amount" min=0 ></td>

                                                </tr>
                                                <tr>
                                                    <td>Paid Amount</td>
                                                    <td><input type="number" id="paidAmount" name="paid_amount" class="form-control" onkeyup="updateAmounts()" min=0 placeholder="Enter paid amount"></td>
                                                </tr>
                                                <tr>
                                                    <th>Amount Due</th>
                                                    <td><input type="number" id = "AmountDue" class="form-control" min=0 value ="0" disabled></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info btn-fill pull-right">Register Bill</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

@endsection


