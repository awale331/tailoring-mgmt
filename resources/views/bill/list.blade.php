@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-plain">
                        <div class="header">
                            @include('inc.message')
                            <h4 class="title">Product Descriptions</h4>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped" id="billTable">
                                <thead>
                                <tr>
                                    <th>Bill Number</th>
                                    <th>Customer Name</th>
                                    <th>Phone Number</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bills as $bill)
                                    <tr>
                                        <td>{{$bill->bill_number}}</td>
                                        <td>{{$bill->customer_name}}</td>
                                        <td>{{$bill->customer_phone_number}}</td>
                                        <td>
                                            <a href="{{ route('bill_edit' ,$bill->id) }}" class="btn btn-primary btn-sm">Update Payment</a>
                                            {{--<form onsubmit="return confirm('Are you sure you want to delete?')" action="{{action('ProductController@destroy',$list->id)}}" method="post" style="display:inline">--}}
                                                {{--@csrf--}}
                                                {{--<button class="btn btn-danger btn-sm">Delete</button>--}}
                                            {{--</form>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(function() {
            $('#bill-list').addClass('active');
        });

        $(document).ready( function () {
            $('#billTable').DataTable();
        } );
    </script>
@endsection
