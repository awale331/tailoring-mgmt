@extends('layouts.layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-plain">
                        <div class="header">
                            @include('inc.message')
                            <h4 class="title">Moods Tailoring</h4>
                            <p class="category">List of worker's in MOODS TAILORING</p>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover" id="workerTable">
                                <thead>
                                <tr>
                                    <th>Bill number</th>
                                    <th>Product</th>
                                    <th>Assigned date</th>
                                    <th>Receiving date</th>
                                    <th>Trial</th>
                                    <th>Delivery</th>
                                    <th>Worker</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lists as $list)
                                    <tr>
                                        <td>{{$list->bill_number}}</td>
                                        <td>{{$list->name}}</td>
                                        <td>{{$list->assigned_date}}</td>
                                        <td>{{$list->receiving_date}}</td>
                                        <td>{{$list->trial_date}}</td>
                                        <td>{{$list->delivery_date}}</td>
                                        <td>@if($list->worker)
                                                {{$list->worker}}
                                            @else
                                                <ul class="nav navbar-nav ">
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                            <p>
                                                                <span class="btn-link">Assign</span>
                                                            </p>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li style="padding: 10px">
                                                                <form action="{{ route('order_store') }}" method="post">
                                                                    @csrf
                                                                    Worker:
                                                                    <select name="worker_id" style="margin-bottom: 5%">
                                                                    @foreach($workers as $worker)
                                                                        <option value="{{$worker->id}}">{{$worker->name}}</option>
                                                                    @endforeach
                                                                    </select>
                                                                    Receiving date:
                                                                    <input type="date" name="receiving_date" style="margin-bottom: 5%" max="{{$list->delivery_date}}">
                                                                    <input type="hidden" name="order_id" value="{{$list->order_id}}">
                                                                    <input type="submit" value="Assign"/>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $(function() {
            $('#order-list').addClass('active');
        });

        $(document).ready( function () {
            $('#workerTable').DataTable();
        } );
    </script>
@endsection
