<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function (){
    return view('login');
})->name('loginpage');

Route::get('/dashboard', 'DashboardController@dashboard') -> name('dashboard');

/*Route for worker*/
Route::get('/worker/create', 'WorkerController@create')-> name('worker_create');
Route::post('/worker/create', 'WorkerController@store')-> name('worker_store');
Route::get('/worker/list', 'WorkerController@index')-> name('worker_list');
Route::get('/worker/{id}/edit', 'WorkerController@edit')-> name('worker_edit');
Route::post('/worker/{id}/edit', 'WorkerController@update')-> name('worker_update');
Route::post('/worker/{id}/destroy', 'WorkerController@destroy')-> name('worker_destroy');
//Route::resource('worker','WorkerController');

/*Route for product*/
Route::get('/product/list', 'ProductController@index')-> name('product_list');
Route::get('/product/create', 'ProductController@create')-> name('product_create');
Route::post('/product/create', 'ProductController@store')-> name('product_store');
Route::get('/product/edit/{id}', 'ProductController@edit')-> name('product_edit');
Route::post('/product/edit/{id}', 'ProductController@update')-> name('product_update');
Route::post('/product/destroy/{id}', 'ProductController@destroy')-> name('product_destroy');

/*Route for user*/
Route::get('/user/create', 'UserController@create')-> name('user_create');
Route::post('/user/create', 'UserController@store')-> name('user_store');
Route::get('/user/list', 'UserController@index')-> name('user_list');
Route::get('/user/{id}/edit', 'UserController@edit')-> name('user_edit');
Route::post('/user/{id}/edit', 'UserController@update')-> name('user_update');
Route::post('/user/{id}/destroy', 'UserController@destroy')-> name('user_destroy');

//Route for Bill
Route::get("/bill/list", "BillController@index")-> name('bill_list');
Route::get("/bill/create", "BillController@create")-> name('bill_create');
Route::post("/bill/create", "BillController@store")-> name('bill_store');
Route::get("/bill/edit/{id}", "BillController@edit")-> name('bill_edit');
Route::post("/bill/edit/{id}", "BillController@update")-> name('bill_update');

//Route for Order assignments
Route::get("/order/list", "AssignmentController@index")-> name('order_list');
Route::post("/order/assign", "AssignmentController@store")-> name('order_store');

//Routes for Login
Route::post("/login", "LoginController@login") -> name('userlogin');
Route::get("/logout", "LoginController@logout")->name('logout');
