<?php
/**
 * Created by PhpStorm.
 * User: vhndaree
 * Date: 3/13/19
 * Time: 2:00 PM
 */

namespace App\Libraries;

class Services
{
    public function getPreviousSevenDays()
    {
        $date=array();
        $timestamp = time()-604800;
        for ($i = 0 ; $i < 7 ; $i++) {
            $date[$i]= date('Y-m-d', $timestamp+=86400);
        }

        return $date;
    }
}