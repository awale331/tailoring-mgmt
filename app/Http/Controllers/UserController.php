<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Storage;
use DB;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('loginCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->get('userInstance')->role == "ADMIN") {
            $user = User::all();
        } else if($request->session()->get('userInstance')->role == "MANAGER"){
            $user=User::where('role','!=','ADMIN')->get();
        }else {
            $user=User::where('id', '=', $request->session()->get('userInstance')->id)->get();
        }

        return view('user.list')->with('lists',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name'=>'required',
            'password'=>'required',
            'phone_number'=>'required|min:7|max:10'
        ]);

        user::create([
            'name'=>$request->name,
            'password'=>Hash::make($request->password),
            'phone_number'=>$request->phone_number,
        ]);

        return redirect('/user/list')->with('success','User added successfully.');
    }

    public function show(User $user)
    {
        //
    }


    public function edit($id)
    {
        $old = User::find($id);
        return view('user.editList')->with('old',$old);
    }


    public function update(Request $request,$id)
    {
        $this->validate(request(),[
            'name'=>'required',
            'phone_number'=>'required|min:10'
        ]);

        $user= User::find($id);

        $user->name= $request->input('name');

        if($request->input('password') != null){
            $user->password= Hash::make($request->input('password'));
        }

        $user->phone_number= $request->input('phone_number');
        // add other fields
        $user->save();

        return redirect('user/list')->with('success','User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('user/list')->with('success', 'User deleted Successfully');
    }
}
