<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Worker;
use Illuminate\Http\Request;
use DB;
use Exception;

class AssignmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('loginCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers=DB::table('workers')->select('id','name')->get();
        $assignments=DB::table('assignments')
                    ->rightJoin('workers', 'assignments.worker_id', '=', 'workers.id')
                    ->rightJoin('orders', 'assignments.order_id', '=', 'orders.id')
                    ->join('products', 'orders.product_id', '=', 'products.id')
                    ->join('bills','orders.bill_id','=','bills.id')
                    ->select('workers.name as worker', 'products.name', 'bills.bill_number','orders.id as order_id','assignments.assigned_date', 'assignments.receiving_date', 'orders.trial_date', 'orders.delivery_date')
                    ->get();
        return view('order.list',['lists'=>$assignments, 'workers'=>$workers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!DB::table("orders")->where("id", $request->order_id)->first()) {
            throw new Exception('Invalid order');
        }

        if (!DB::table("workers")->where("id", $request->worker_id)->first()) {
            throw new Exception('Invalid worker selected');
        }

        $this->validate(request(),[
            'worker_id'=>"required",
            'receiving_date'=>"required|after_or_equal:today",
            'order_id'=>"required",
            ]);

        date_default_timezone_set("Asia/Kathmandu");

        Assignment::create([
            'order_id'=>$request->order_id,
            'worker_id'=>$request->worker_id,
            'assigned_date'=>date('Y-m-d'),
            'receiving_date'=>$request->receiving_date,
        ]);

        return redirect('/order/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        //
    }
}
