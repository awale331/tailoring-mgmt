<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Product;
use App\Order;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Libraries\NepaliCalendar;


class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginCheck');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = Bill::all();
        return view("bill/list",["bills"=>$bills]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $prevBillNumber = DB::table('bills')->max('bill_number');
        if($prevBillNumber == null || $prevBillNumber == 0){
            $newBillNumber = 1;
        }else{
            $newBillNumber = $prevBillNumber + 1;
        }
    return view("bill/form",["billNumber"=>$newBillNumber, "products"=>$products]);
    }


    public function store(Request $request)
    {
     /*   $oldDate = strtotime($request->delivery_date);
        $cal = new NepaliCalendar();
        dd($cal->eng_to_nep(date("Y", $oldDate), date("m", $oldDate), date("d", $oldDate)));*/

        $this->validate(request(),[
            'bill_number'=>'required|unique:bills',
            'customer_name'=>'required|max:50',
            'customer_number'=>'required|numeric|digits_between:6,11',
            'products'=>'required',
            'quantity'=>'required',
            'wage_rate'=>'required',
            'total_amount'=>'required|numeric|gt:0',
            'paid_amount'=>'required|numeric|lte:total_amount|gte:0',
            'trial_date'=>'required|date|after_or_equal:today',
            'delivery_date'=>'required|date|after_or_equal:trial_date',
        ]);

        DB::transaction(function () use ($request){
            $bill = Bill::create([
                'bill_number'=>$request->bill_number,
                'customer_name'=>$request->customer_name,
                'customer_phone_number'=>$request->customer_number,
                'total_amount'=>$request->total_amount,
            ]);

            if($request->paid_amount > 0){
                Payment::create([
                    'bill_id'=>$bill->id,
                    'amount'=>$request->paid_amount,
                ]);
            }

            for($i=0; $i<sizeOf($request->products); $i++ ){
                if (!DB::table("products")->where("id", $request->products[$i])->first()) {
                    throw new Exception('Invalid product selected');
                }
                Order::create([
                    'product_id'=>$request->products[$i],
                    'bill_id'=>$bill->id,
                    'quantity'=>$request->quantity[$i],
                    'wage_rate'=>$request->wage_rate[$i],
                    'trial_date'=>$request->trial_date,
                    'delivery_date'=>$request->delivery_date,
                ]);
            }
        }, 5);
        return redirect('/bill/create')->with('success','Bill created successfully.');
    }



    public function show(Bill $bill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();
        $bill = Bill::find($id);
        $bill_id = $bill->id;
        $orders = Order::all();
        $od= Order::where('bill_id',$bill_id)->first();
        $payment = Payment::where('bill_id',$bill_id)->sum('amount');
        return view('bill/editBill',['orders'=>$orders, 'bill'=>$bill, 'payment'=>$payment, 'products'=>$products,'od'=>$od]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {   $bills = Bill::whereid($id)->first();
        $bill_id = $bills->id;
        $paid = Payment::where('bill_id',$bill_id)->sum('amount');
        $total = $bills->total_amount;
        $messages = [
            'lte' => 'The :attribute doesnt match with previous amount.',
            'gte' => 'The :attribute doesnt match with previous amount.'
        ];
        $this->validate(request(),[
            'total_amount'=>'required|numeric|lte:'.$total.'|gte:'.$total,
            'paid_amount'=>'numeric|lte:total_amount|lte:'.$paid.'|gte:'.$paid,
            'new_amount'=>'numeric|lte:total_amount|gte:0',
        ], $messages);
        if($request->paid_amount > 0){
            Payment::create([
                'bill_id'=>$id,
                'amount'=>$request->new_amount,
            ]);
        }
        return redirect('bill/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bill $bill)
    {
        //
    }
}
