<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        $user = User::where('phone_number', $request->username)->first();
        if($user && Hash::check($request->password, $user->password)){
            session(['userInstance' => $user]);
            return redirect('/dashboard');
        } else {
            return back()->with('error','Please enter valid credential.');
        }
    }

    public function logout(Request $request){
        $request->session()->forget('userInstance');
        return redirect()->route('loginpage');
    }
}
