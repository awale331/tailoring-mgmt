<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Storage;
use DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginCheck');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $product=Product::all();
       return view('product.list')->with('lists',$product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
           'name'=>'required|max:50',
           'description'=>'required',
           'wage_rate'=>'required|numeric|gte:0'
        ]);

        product::create([
            'name'=>$request->name,
            'description'=>$request->description,
            'wage_rate'=>$request->wage_rate,
        ]);

        return redirect('/product/list')->with('success','Product added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $old=Product::find($id);
        return view('product.editProduct')->with('old',$old);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate(request(),[
            'name'=>'required',
            'description'=>'required',
            'wage_rate'=>'required'
        ]);

        $product=Product::find($id);

        $product->name=$request->input('name');
        $product->wage_rate=$request->input('wage_rate');
        $product->description=$request->input('description');
        $product->save();
        return redirect('product/list')->with('success','Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product= Product::find($id);
        $product->delete();

        return redirect ('product/list')->with('success', 'Product deleted Successfully');
    }
}
