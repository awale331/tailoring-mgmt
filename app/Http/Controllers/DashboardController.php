<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use DB;
use Carbon;
use App\Libraries\Services;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginCheck');
    }

    public function dashboard(){
            $salesAndRevenue=new DashboardController();
            //dd($salesAndRevenue->salesAndRevenue());
            $todayTrials= DashboardController::todayTrials();
            $todayDeliveries = DashboardController::todayDeliveries();
            $todayCollections = DashboardController::todayCollections();
            return view('welcome',[
                'salesAndRevenue'=>$salesAndRevenue->salesAndRevenue(),
                "todayTrials"=>$todayTrials,
                "todayDeliveries"=>$todayDeliveries,
                "todayCollections"=>$todayCollections
            ]);
    }

    public function todayDeliveries()
    {
        date_default_timezone_set("Asia/Kathmandu");
        $date = date("Y-m-d");
        $todayDeliveries = DB::table('orders')
            ->join('bills', 'orders.bill_id', '=', 'bills.id')
            ->join('products', 'orders.product_id', '=', 'products.id')
            ->select('bills.bill_number', 'bills.customer_name', 'bills.customer_phone_number', 'products.name as items')
            ->where('orders.delivery_date', '=', $date)
            ->get();
        return DashboardController::concatenateItems($todayDeliveries);
    }

    public static function todayTrials()
    {
        date_default_timezone_set("Asia/Kathmandu");
        $trials = DB::table("orders")
            ->join("products", "orders.product_id", "=", "products.id")
            ->join("bills", "orders.bill_id", "=", "bills.id")
            ->select("bills.bill_number", "bills.customer_name", "bills.customer_phone_number", "products.name as items")
            ->where("orders.trial_date", '=', date("Y-m-d"))
            ->get();
        return DashboardController::concatenateItems($trials);;
    }

    public static function todayCollections(){
        date_default_timezone_set("Asia/Kathmandu");
        $collections = DB::table("bills")
            ->join("orders","bills.id",'=','orders.bill_id')
            ->join("products","products.id","=","orders.product_id")
            ->join("assignments","orders.id","=", "assignments.order_id")
            ->join("workers","workers.id","=","assignments.worker_id")
            ->where("receiving_date","=",date("Y-m-d"))
            ->select("bills.bill_number","bills.customer_name","workers.name as worker", "workers.phone_number","products.name as item")
            ->get();

        return $collections;
    }

    public function salesAndRevenue(){
        $date = new Carbon\Carbon;
        $sales= DB::table('bills')
                ->select(DB::raw('DATE(bills.created_at) as date'), DB::raw('sum(bills.total_amount) as total_sales'))
                ->groupBy(DB::raw('DATE(bills.created_at)'))
                ->where(DB::raw('DATE(bills.created_at)'),'>',$date->subDays(7)->toDateTimeString())
                ->get()->toArray();

        $revenue= DB::table('payments')
                ->select(DB::raw('DATE(payments.created_at) as date'), DB::raw('sum(payments.amount) as total_revenue'))
                ->groupBy(DB::raw('DATE(payments.created_at)'))
                ->where(DB::raw('DATE(payments.created_at)'),'>',$date->subDays(7)->toDateTimeString())
                ->get()->toArray();
        $salesAndRevenue=array();

        // Previous 7 days date function from Libraries/Services
        $service=new Services();
        $date=$service->getPreviousSevenDays();
        //end of service

        for($i=0;$i<sizeof($date);$i++){
            for($j=0;$j<sizeof($sales);$j++) {
                if($date[$i]==$sales[$j]->date){
                    $salesAndRevenue[$i]=["date"=>$date[$i], "total_sales"=>$sales[$j]->total_sales];
                    break;
                } else {
                    $salesAndRevenue[$i]=["date"=>$date[$i], "total_sales"=>0];
                }
            }

            for($j=0;$j<sizeof($revenue);$j++) {
                if($date[$i]==$revenue[$j]->date){
                    $salesAndRevenue[$i]['total_revenue']=$revenue[$j]->total_revenue;
                    break;
                } else {
                    $salesAndRevenue[$i]['total_revenue']=0;
                }
            }
        }

        return $salesAndRevenue;
    }
    public static function concatenateItems($redundantRecords){
        $optimized_records = [];
        $count = 0;
        for( $i= 0; $i< count($redundantRecords); $i++){
            if($redundantRecords[$i] != null){
                $record = $redundantRecords[$i];
                for($j = $i+1; $j < count($redundantRecords); $j++){
                    if($record->bill_number == $redundantRecords[$j]->bill_number){
                        $record->items = $record->items.", ".$redundantRecords[$j]->items;
                        $redundantRecords[$j] = null;
                    }
                }
                $optimized_records[$count] = $record;
                $count++;
            }
        }

        return $optimized_records;
    }
}
