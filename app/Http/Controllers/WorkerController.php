<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginCheck');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $worker = Worker::all();
        return view('worker.list')->with('lists',$worker);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('worker.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'name'=>'required',
            'address'=>'required',
            'phone_number'=>'required|numeric|digits_between:6,11'
        ]);

        $worker= new Worker();

        $worker->name= $request->input('name');
        $worker->address= $request->input('address');
        $worker->phone_number= $request->input('phone_number');
        // add other fields
        $worker->save();

        return redirect('/worker/list')->with('success','User added to the list successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $old = Worker::find($id);
        return view('worker.editList')->with('old',$old);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate(request(),[
            'name'=>'required|max:50',
            'address'=>'required|max:255',
            'phone_number'=>'required|numeric|digits_between:6,11'
        ]);

        $worker= Worker::find($id);

        $worker->name= $request->input('name');
        $worker->address= $request->input('address');
        $worker->phone_number= $request->input('phone_number');
        // add other fields
        $worker->save();

        return redirect('worker/list')->with('success','User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $worker = Worker::find($id);
        $worker->delete();

        return redirect('worker/list')->with('success', 'Worker has been deleted Successfully');
    }
}
