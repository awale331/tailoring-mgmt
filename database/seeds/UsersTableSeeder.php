<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "tailor";
        $user->phone_number = "0123456789";
        $user->password = Hash::make("tailor");
        $user->save();
    }
}
